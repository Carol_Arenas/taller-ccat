package com.example.postgresdemo.service;

import com.example.postgresdemo.model.Quiz;
import com.example.postgresdemo.repository.AnswerRepository;
import com.example.postgresdemo.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface QuizService {

    Page<Quiz> getQuizzes(Pageable pageable);

    Optional<Quiz> getQuiz(Long idQuiz);

    Quiz createQuiz(Quiz question);

    Quiz updateQuiz(Long questionId, Quiz questionRequest);

    ResponseEntity<?> deleteQuiz(Long questionId);
}
