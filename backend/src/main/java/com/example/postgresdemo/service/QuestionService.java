package com.example.postgresdemo.service;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Question;
import com.example.postgresdemo.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

@Service
public class QuestionService {
    @Autowired
    private QuestionRepository questionRepository;


    public Page<Question> getQuestions(Pageable pageable) {
        return questionRepository.findAll(pageable);
    }

    public Optional<Question> getQuestion(Long idQuestion) {
        return questionRepository.findById(idQuestion);
    }

    public Question createQuestion(Question question) {
        return questionRepository.save(question);
    }


    public Question updateQuestion(Long questionId, Question questionRequest) {
        Question response = questionRepository.findById(questionId)
                .map(question -> {
                    question.setTitle(questionRequest.getTitle());
                    question.setDescription(questionRequest.getDescription());
                    return questionRepository.save(question);
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
        return response;
    }


    public ResponseEntity<?> deleteQuestion( Long questionId) {
        ResponseEntity<Object> objectResponseEntity = questionRepository.findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
        return objectResponseEntity;
    }
}
