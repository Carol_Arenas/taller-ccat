package com.example.postgresdemo.service;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Answer;
import com.example.postgresdemo.model.Question;
import com.example.postgresdemo.repository.AnswerRepository;
import com.example.postgresdemo.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;


@Service
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private QuestionRepository questionRepository;

    public List<Answer> getAnswersByQuestionId( Long questionId) {
        return answerRepository.findByQuestionId(questionId);
    }

    public Answer addAnswer(Long questionId, Answer answer) {
        return questionRepository.findById(questionId)
               .map(question -> {
                   answer.setQuestion(question);
                   return answerRepository.save(answer);
               }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    public Answer updateAnswer( Long questionId, Long answerId, Answer answerRequest) {
        if(!questionRepository.existsById(questionId)) {
            throw new ResourceNotFoundException("Question not found with id " + questionId);
        }

        return answerRepository.findById(answerId)
                .map(answer -> {
                    answer.setText(answerRequest.getText());
                    return answerRepository.save(answer);
                }).orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + answerId));
    }

    public ResponseEntity<?> deleteAnswer(Long questionId, Long answerId) {
        if(!questionRepository.existsById(questionId)) {
            throw new ResourceNotFoundException("Question not found with id " + questionId);
        }

        return answerRepository.findById(answerId)
                .map(answer -> {
                    answerRepository.delete(answer);
                    return ResponseEntity.ok().build();
                }).orElseThrow(new Supplier<ResourceNotFoundException>() {
                    @Override
                    public ResourceNotFoundException get() {
                        return new ResourceNotFoundException("Answer not found with id " + answerId);
                    }
                });

    }
}
