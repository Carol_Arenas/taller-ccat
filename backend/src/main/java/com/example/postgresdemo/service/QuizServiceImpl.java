package com.example.postgresdemo.service;

import com.example.postgresdemo.model.Quiz;
import com.example.postgresdemo.repository.AnswerRepository;
import com.example.postgresdemo.repository.QuestionRepository;
import com.example.postgresdemo.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class QuizServiceImpl implements  QuizService{
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private QuizRepository quizRepository;

    @Override
    public Page<Quiz> getQuizzes(Pageable pageable) {
        return quizRepository.findAll(pageable);
    }

    @Override
    public Optional<Quiz> getQuiz(Long idQuiz) {
        return quizRepository.findById(idQuiz);
    }

    @Override
    public Quiz createQuiz(Quiz quiz) {
        return quizRepository.save(quiz);
    }

    @Override
    public Quiz updateQuiz(Long questionId, Quiz questionRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> deleteQuiz(Long questionId) {
        return null;
    }
}
