package com.example.postgresdemo.controller;

import com.example.postgresdemo.model.Quiz;
import com.example.postgresdemo.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class QuizController {
    @Autowired
    private QuizService quizService;


    @GetMapping("/quizzes")
    public Page<Quiz> getQuizzes(Pageable pageable) {
        return quizService.getQuizzes(pageable);
    }

    @GetMapping("/quiz/{idQuiz}")
    public Optional<Quiz> getQuiz(@PathVariable Long idQuiz) {

        return quizService.getQuiz(idQuiz);
    }

    @PostMapping("/quiz")
    public Quiz createQuiz(@Valid @RequestBody Quiz quiz) {
        return quizService.createQuiz(quiz);
    }

    @PutMapping("/quiz/{quizId}")
    public Quiz updateQuiz(@PathVariable Long quizId,
                                   @Valid @RequestBody Quiz quizRequest) {
        return quizService.updateQuiz(quizId, quizRequest);
    }


    @DeleteMapping("/quiz/{quizId}")
    public ResponseEntity<?> deleteQuiz(@PathVariable Long quizId) {
        return quizService.deleteQuiz(quizId);
    }
}
